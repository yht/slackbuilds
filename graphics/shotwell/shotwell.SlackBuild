#!/bin/sh

# Brad Hermanson <apeitheo@gmail.com>

PRGNAM=shotwell
VERSION=0.11.6
BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
    *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.bz2
cd $PRGNAM-$VERSION

chown -R root:root .
chmod -R u+w,go+r-w,a-s .

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --lib=lib${LIBDIRSUFFIX} \
  --disable-schemas-compile \
  --disable-desktop-update \
  --disable-icon-update \
  --enable-build-for-glade \
  --install-headers \
  --release

make
make install DESTDIR=$PKG

# Yes, we're going to overwrite the Slackware-provided rules file.
# 1. It's incorrect (it should have been generated this way in the 
#    libgphoto2 package, but blame rworkman for that)  :)
# 2. Shotwell needs this to function correctly
# 3. It won't hurt at all to leave it if/when shotwell is removed (see #1)
mkdir -p $PKG/lib/udev/rules.d
print-camera-list udev-rules version 136 mode 0660 owner root group plugdev > \
  $PKG/lib/udev/rules.d/90-libgphoto2.rules

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  AUTHORS COPYING INSTALL MAINTAINERS NEWS README THANKS \
  $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}
